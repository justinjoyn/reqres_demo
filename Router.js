import React from "react";
import {createBottomTabNavigator, createStackNavigator} from "react-navigation";
import Icon from 'react-native-vector-icons/MaterialIcons';

import {Splash} from "./modules/splash";
import {Login} from "./modules/login";
import {Users} from "./modules/users";
import {Colors} from "./modules/colors";

const BottomTabNavigator = createBottomTabNavigator({
    Users: {
        screen: Users,
        navigationOptions: {
            tabBarLabel: 'Users',
            tabBarIcon: ({tintColor}) => (
                <Icon name="person-outline" size={22} color={tintColor}/>
            )
        }
    },
    Colors: {
        screen: Colors,
        navigationOptions: {
            tabBarLabel: 'Colors',
            tabBarIcon: ({tintColor}) => (
                <Icon name="palette" size={22} color={tintColor}/>
            )
        }
    }
});

const StackNavigator = createStackNavigator(
    {
        Splash: {
            screen: Splash
        },
        Login: {
            screen: Login
        },
        Home: {
            screen: BottomTabNavigator
        }
    },
    {
        initialRouteName: "Splash",
        headerMode: 'none'
    }
);

export default StackNavigator;
