export default {
    "empty_user_list_heading": "Aún no hay usuarios",
    "empty_user_list_message": "Tire hacia abajo para actualizar la lista de usuarios de nuevo...",

    "empty_color_list_heading": "No hay colores todavía\n",
    "empty_color_list_message": "Tire hacia abajo para actualizar la lista de colores de nuevo...",
}
