import React from "react";

export default {
    "empty_user_list_heading": "No users yet",
    "empty_user_list_message": "Pull down to refresh users list again...",

    "empty_color_list_heading": "No colors yet",
    "empty_color_list_message": "Pull down to refresh colors list again...",
}
