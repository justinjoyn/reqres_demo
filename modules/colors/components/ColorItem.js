import React from "react";
import {StyleSheet, Text, View} from "react-native";

export const ColorItem = props => {
    const {color} = props;
    console.log(color.color);

    return (
        <View style={[styles.item, {backgroundColor: color.color}]}>
            <View style={styles.tags}>
                <View style={styles.tagsView}>
                    <Text style={styles.tag}>{color.name}</Text>
                </View>
                <View style={styles.tagsView}>
                    <Text style={styles.tag}>{color.color}</Text>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    item: {
        flexDirection: "row",
        padding: 10,
        marginVertical: 5,
        borderColor: '#CCC',
        borderWidth: 1,
        borderRadius: 8,
        marginHorizontal: 10,
        height: 100,
        alignItems: 'flex-end'
    },
    tags: {
        flexDirection: 'row'
    },
    tagsView: {
        paddingHorizontal: 10,
        borderRadius: 20,
        borderColor: '#FFF',
        borderWidth: 1,
        height: 25,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(0,0,0,0.2)',
        marginRight: 10
    },
    tag: {
        fontSize: 12,
        color: '#FFF'
    }
});
