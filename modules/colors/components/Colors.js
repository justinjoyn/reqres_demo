import React, {Component} from "react";
import {FlatList, StatusBar, StyleSheet, Text, View} from "react-native";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Icon from "react-native-vector-icons/MaterialIcons";
import {SafeAreaView} from "react-navigation";
import {ColorItem} from "./ColorItem";
import {getColors} from "../actions";
import I18n from "../../../i18n/i18n";

class Colors extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (!this.props.colorsList)
            this.props.getColors();
    }

    _keyExtractor = (item, index) => "order_" + item.id;

    _handleRefresh = () => {
        this.props.getColors();
    };

    _renderListEmpty() {
        return (
            <View style={styles.emptyView}>
                <Icon name="sync" size={80}/>
                <Text style={styles.emptyHeading}>{I18n.t('empty_color_list_heading')}</Text>
                <Text style={styles.emptyMessage}>{I18n.t('empty_color_list_message')}</Text>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content"/>
                <FlatList
                    style={styles.colorList}
                    refreshing={this.props.isLoading}
                    ListEmptyComponent={this.props.isLoading ? null : this._renderListEmpty}
                    onRefresh={this._handleRefresh}
                    data={this.props.colorsList}
                    keyExtractor={this._keyExtractor}
                    renderItem={({item}) => (
                        <ColorItem color={item}/>
                    )}/>
            </SafeAreaView>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        getColors
    }, dispatch);
};

const mapStateToProps = state => ({
    colorsList: state.colors.colors.data,
    isLoading: state.colors.colors.isLoading
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Colors);

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: '#FFF'
    },
    colorList: {
        flex: 1
    },
    emptyView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 500
    },
    emptyHeading: {
        marginTop: 10,
        fontSize: 20
    },
    emptyMessage: {
        fontSize: 14
    }
});
