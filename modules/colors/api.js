import axios from "axios";
import {makeURL} from "../../common/utils";
import {COLORS} from "../../common/endpoints";

export const getColors$$ = payload => {
    return axios({
        method: "GET",
        url: makeURL(COLORS) + '?per_page=12',
        data: payload
    }).then(response => response);
};
