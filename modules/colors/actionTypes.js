const prefix = `COLORS_`;

export const GET_COLORS = `${prefix}GET_COLORS`;
export const GET_COLORS_SUCCESS = `${prefix}GET_COLORS_SUCCESS`;
export const GET_COLORS_FAILURE = `${prefix}GET_COLORS_FAILURE`;
