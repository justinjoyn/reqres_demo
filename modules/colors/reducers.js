import {GET_COLORS, GET_COLORS_FAILURE, GET_COLORS_SUCCESS,} from "./actionTypes";

export const colors = (state = {data: null, error: null, isLoading: false}, {type, payload}) => {
    switch (type) {
        case GET_COLORS:
            return {
                ...state,
                isLoading: true
            };
        case GET_COLORS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                data: payload.data
            };
        case GET_COLORS_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: payload.error
            };
        default:
            return state;
    }
};
