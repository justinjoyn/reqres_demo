import Colors from "./components/Colors";
import * as epics from "./epics";
import * as reducers from "./reducers";
import {combineReducers} from "redux";

export const colorsEpics = Object.values(epics);

export const colors = combineReducers({
    ...reducers
});

export {
    Colors
};
