import {actionCreator} from "../../common/utils";
import {GET_COLORS, GET_COLORS_FAILURE, GET_COLORS_SUCCESS,} from "./actionTypes";

export const getColors = actionCreator(GET_COLORS);
export const getColorsSuccess = actionCreator(GET_COLORS_SUCCESS);
export const getColorsFailure = actionCreator(GET_COLORS_FAILURE);
