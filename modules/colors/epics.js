import {catchError, map, switchMap, tap} from "rxjs/operators";
import {fromPromise} from "rxjs/observable/fromPromise";
import {of} from "rxjs/observable/of";
import {ofType} from "redux-observable";

import {GET_COLORS} from "./actionTypes";
import {getColorsFailure, getColorsSuccess} from "./actions";
import {handleResponse} from "../../common/utils";
import {getColors$$} from "./api";

export const getColors$ = action$ => {
    return action$.pipe(
        ofType(GET_COLORS),
        switchMap(({payload}) => {
            return fromPromise(getColors$$(payload)).pipe(
                tap(console.log),
                map(response =>
                    handleResponse(response, getColorsSuccess, getColorsFailure)
                ),
                catchError(err => of(getColorsFailure(err)))
            );
        })
    );
};
