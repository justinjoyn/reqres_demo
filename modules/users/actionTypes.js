const prefix = `USERS_`;

export const GET_USERS = `${prefix}GET_USERS`;
export const GET_USERS_SUCCESS = `${prefix}GET_USERS_SUCCESS`;
export const GET_USERS_FAILURE = `${prefix}GET_USERS_FAILURE`;
