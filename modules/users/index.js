import Users from "./components/Users";
import * as epics from "./epics";
import * as reducers from "./reducers";
import {combineReducers} from "redux";

export const usersEpics = Object.values(epics);

export const users = combineReducers({
    ...reducers
});

export {
    Users
};
