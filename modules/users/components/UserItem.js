import React from "react";
import {Image, StyleSheet, Text, View} from "react-native";

export const UserItem = props => {
    const {user} = props;
    return (
        <View style={styles.item}>
            <Image source={{uri: user.avatar}} style={styles.avatar} resizeMode={"contain"}/>
            <View style={styles.nameView}>
                <Text style={styles.name}>{user.first_name + ' ' + user.last_name}</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    item: {
        flexDirection: "row",
        padding: 5,
        marginVertical: 5,
        borderColor: '#CCC',
        borderWidth: 1,
        borderRadius: 8,
        marginHorizontal: 10,
    },
    avatar: {
        height: 50,
        width: 50,
        borderRadius: 30
    },
    nameView: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 20
    },
    name: {
        fontSize: 14,
        fontWeight: '100'
    }
});
