import React, {Component} from "react";
import {FlatList, StatusBar, StyleSheet, Text, View} from "react-native";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getUsers} from "../actions";
import {UserItem} from "./UserItem";
import Icon from "react-native-vector-icons/MaterialIcons";
import {SafeAreaView} from "react-navigation";
import I18n from "../../../i18n/i18n";

class Users extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (!this.props.usersList)
            this.props.getUsers();
    }

    _keyExtractor = (item, index) => "order_" + item.id;

    _handleRefresh = () => {
        this.props.getUsers();
    };

    _renderListEmpty() {
        return (
            <View style={styles.emptyView}>
                <Icon name="sync" size={80}/>
                <Text style={styles.emptyHeading}>{I18n.t('empty_user_list_heading')}</Text>
                <Text style={styles.emptyMessage}>{I18n.t('empty_user_list_message')}</Text>
            </View>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content"/>
                <FlatList
                    style={styles.userList}
                    refreshing={this.props.isLoading}
                    ListEmptyComponent={this.props.isLoading ? null : this._renderListEmpty}
                    onRefresh={this._handleRefresh}
                    data={this.props.usersList}
                    keyExtractor={this._keyExtractor}
                    renderItem={({item}) => (
                        <UserItem user={item}/>
                    )}/>
            </SafeAreaView>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        getUsers
    }, dispatch);
};

const mapStateToProps = state => ({
    usersList: state.users.users.data,
    isLoading: state.users.users.isLoading
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Users);

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: '#FFF'
    },
    userList: {
        flex: 1
    },
    emptyView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        height: 500
    },
    emptyHeading: {
        marginTop: 10,
        fontSize: 20
    },
    emptyMessage: {
        fontSize: 14
    }
});
