import {catchError, map, switchMap, tap} from "rxjs/operators";
import {fromPromise} from "rxjs/observable/fromPromise";
import {of} from "rxjs/observable/of";
import {ofType} from "redux-observable";

import {GET_USERS} from "./actionTypes";
import {getUsersFailure, getUsersSuccess} from "./actions";
import {handleResponse} from "../../common/utils";
import {getUsers$$} from "./api";

export const getUsers$ = action$ => {
    return action$.pipe(
        ofType(GET_USERS),
        switchMap(({payload}) => {
            return fromPromise(getUsers$$(payload)).pipe(
                tap(console.log),
                map(response =>
                    handleResponse(response, getUsersSuccess, getUsersFailure)
                ),
                catchError(err => of(getUsersFailure(err)))
            );
        })
    );
};
