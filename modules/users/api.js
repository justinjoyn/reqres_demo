import axios from "axios";
import {makeURL} from "../../common/utils";
import {USERS} from "../../common/endpoints";

export const getUsers$$ = payload => {
    return axios({
        method: "GET",
        url: makeURL(USERS) + '?per_page=12',
        data: payload
    }).then(response => response);
};
