import {GET_USERS, GET_USERS_FAILURE, GET_USERS_SUCCESS,} from "./actionTypes";

export const users = (state = {data: null, error: null, isLoading: false}, {type, payload}) => {
    switch (type) {
        case GET_USERS:
            return {
                ...state,
                isLoading: true
            };
        case GET_USERS_SUCCESS:
            return {
                ...state,
                isLoading: false,
                data: payload.data
            };
        case GET_USERS_FAILURE:
            return {
                ...state,
                isLoading: false,
                error: payload.error
            };
        default:
            return state;
    }
};
