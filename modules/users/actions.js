import {actionCreator} from "../../common/utils";
import {GET_USERS, GET_USERS_FAILURE, GET_USERS_SUCCESS,} from "./actionTypes";

export const getUsers = actionCreator(GET_USERS);
export const getUsersSuccess = actionCreator(GET_USERS_SUCCESS);
export const getUsersFailure = actionCreator(GET_USERS_FAILURE);
