import {USER_LOGIN, USER_LOGIN_FAILURE, USER_LOGIN_SUCCESS,} from "./actionTypes";

export const user = (state = {token: null, error: null, isLoading: false}, {type, payload}) => {
    switch (type) {
        case USER_LOGIN:
            return {
                ...state,
                isLoading: true
            };
        case USER_LOGIN_SUCCESS:
            return {
                ...state,
                token: payload.token,
                error: null,
                isLoading: false
            };
        case USER_LOGIN_FAILURE:
            return {
                ...state,
                token: null,
                error: payload.error,
                isLoading: false
            };
        default:
            return state;
    }
};
