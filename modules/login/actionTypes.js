const prefix = `LOGIN_`;

export const USER_LOGIN = `${prefix}USER_LOGIN`;
export const USER_LOGIN_SUCCESS = `${prefix}USER_LOGIN_SUCCESS`;
export const USER_LOGIN_FAILURE = `${prefix}USER_LOGIN_FAILURE`;
