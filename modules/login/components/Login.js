import React, {Component} from "react";
import {ActivityIndicator, StatusBar, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {userLogin} from "../actions";
import {NavigationActions, SafeAreaView, StackActions} from "react-navigation";

class Login extends Component {

    state = {
        email: '',
        password: '',
        isEmailValid: false,
        isPasswordValid: false,
        remember: false,
        checkingLogin: true
    };

    static getDerivedStateFromProps(props) {
        if (props.userToken && props.userToken.length > 0) {
            const resetAction = StackActions.reset({
                key: null,
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'Home'})],
            });
            props.navigation.dispatch(resetAction);
        }
        return null;
    }

    validateEmail = () => {
        const emailRegex = /^(.*?)$/;
        return emailRegex.test(this.state.email);
    };

    validatePassword = () => {
        const passwordRegex = /^(.*?)$/;
        return passwordRegex.test(this.state.password);
    };

    onFieldValueChanged = (field, value) => {
        this.setState({
            [field]: value
        });
    };

    onLogin = () => {
        if (this.props.isLoading) return;
        if (this.validateEmail() && this.validatePassword()) {
            this.props.userLogin({
                email: this.state.email,
                password: this.state.password
            });
        } else {
            console.log("Please check the SSN and birth year");
        }
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar backgroundColor="#FFFFFF" barStyle="dark-content"/>
                <View style={styles.card}>
                    <View>
                        <TextInput style={styles.formInput}
                                   autoCapitalize="none"
                                   placeholder="johndoe@gmail.com"
                                   placeholderTextColor="#D1D1D1"
                                   underlineColorAndroid="#D1D1D1"
                                   onChangeText={value =>
                                       this.onFieldValueChanged("email", value)
                                   }
                                   onEndEditing={this.validateEmail}
                                   value={this.state.email}/>
                    </View>
                    <View>
                        <TextInput style={styles.formInput}
                                   placeholder="*********"
                                   placeholderTextColor="#D1D1D1"
                                   underlineColorAndroid="#D1D1D1"
                                   autoCapitalize="none"
                                   secureTextEntry={true}
                                   onChangeText={value =>
                                       this.onFieldValueChanged("password", value)
                                   }
                                   onEndEditing={this.validatePassword}
                                   value={this.state.password}/>
                    </View>
                    <View>
                        <TouchableOpacity onPress={this.onLogin}>
                            <View style={styles.loginButton}>
                                {this.props.isLoading ?
                                    <ActivityIndicator size="small" color="#ffffff"/>
                                    : <Text style={styles.loginButtonText}>Login</Text>}
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        userLogin
    }, dispatch);
};

const mapStateToProps = state => ({
    userToken: state.login.user.token,
    isLoading: state.login.user.isLoading,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        backgroundColor: '#FFF',
        justifyContent: 'center'
    },
    card: {
        padding: 10,
        marginVertical: 5,
        borderColor: '#CCC',
        borderWidth: 1,
        borderRadius: 8,
        marginHorizontal: 10
    },
    formInput: {
        marginVertical: 10
    },
    loginButton: {
        backgroundColor: '#448AFF',
        marginVertical: 10,
        paddingVertical: 10,
        borderRadius: 5,
        alignSelf: 'flex-end',
        width: 100,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loginButtonText: {
        color: '#FFF',
        fontSize: 14
    }
});
