import {actionCreator} from "../../common/utils";
import {USER_LOGIN, USER_LOGIN_FAILURE, USER_LOGIN_SUCCESS,} from "./actionTypes";

export const userLogin = actionCreator(USER_LOGIN);
export const userLoginSuccess = actionCreator(USER_LOGIN_SUCCESS);
export const userLoginFailure = actionCreator(USER_LOGIN_FAILURE);
