import axios from "axios";
import {makeURL} from "../../common/utils";
import {LOGIN} from "../../common/endpoints";

export const userLogin$$ = payload => {
    return axios({
        method: "POST",
        url: makeURL(LOGIN),
        data: payload
    }).then(response => response);
};
