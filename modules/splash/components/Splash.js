import React, {Component} from "react";
import {ImageBackground, StatusBar, StyleSheet} from "react-native";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {NavigationActions, StackActions} from "react-navigation";
import ActivityLoader from "../../common/ActivityLoader";

class Splash extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.props.userToken && this.props.userToken.length > 0) {
            const resetAction = StackActions.reset({
                key: null,
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'Home'})],
            });
            this.props.navigation.dispatch(resetAction);
        } else {
            const resetAction = StackActions.reset({
                key: null,
                index: 0,
                actions: [NavigationActions.navigate({routeName: 'Login'})],
            });
            this.props.navigation.dispatch(resetAction);
        }
    }

    render() {
        return (
            <ImageBackground source={require('../../../assets/images/splash.png')} style={styles.container}>
                <StatusBar backgroundColor={"#214E63"} barStyle={"light-content"}/>
                <ActivityLoader loading={true}/>
            </ImageBackground>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({}, dispatch);
};

const mapStateToProps = state => ({
    userToken: state.login.user.token,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Splash);

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
