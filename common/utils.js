import {AsyncStorage} from "react-native";
import {PROD_URL, TEST_URL} from "./constants";

//Save data to AsyncStorage
export const storeData = async (key, value) => {
    try {
        await AsyncStorage.setItem(key, value);
    } catch (error) {
        alert(error);
    }
};

//Fetch data from AsyncStorage
export const retrieveData = async key => {
    try {
        const value = await AsyncStorage.getItem(key);
        if (value !== null) {
            return value;
        } else {
            return undefined;
        }
    } catch (error) {
        alert(error);
        return undefined;
    }
};

//Delete data from AsyncStorage
export const deleteData = async key => {
    try {
        await AsyncStorage.removeItem(key);
    } catch (error) {
        alert(error);
    }
};

export const actionCreator = type => payload => ({type, payload});

//Build API endpoints
export const makeURL = endPoint => {
    if (__DEV__) {
        return `${TEST_URL}${endPoint}`;
    }
    return `${PROD_URL}${endPoint}`;
};

//Handle API responses
export const handleResponse = (response, successHandler, failureHandler) => {
    if (response.status >= 200 && response.status <= 300) {
        return successHandler(response.data);
    } else {
        return failureHandler(response.data);
    }
};
